using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex.WebView;

public class GameController : MonoBehaviour
{

    public CanvasWebViewPrefab canvasWebViewPrefab;
    public string url;

    // Start is called before the first frame update
    void Start()
    {
        canvasWebViewPrefab.WebView.LoadUrl(url);
    }
}
